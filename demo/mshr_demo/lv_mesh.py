from dolfin import Point, plot, MeshFunction
from mshr import Ellipsoid, Box, generate_mesh


# The plane cutting the base
diam    = 10.0
box = Box(Point(0,-2,-2),Point(diam,diam,diam))
# Generate mesh

# The center of the LV ellipsoid
center_lv = Point(0.0, 0.0, 0.0)
# LV epicardium
el_lv = Ellipsoid(center_lv, 2, 1,1)
# LV endocardium
el_lv_endo = Ellipsoid(center_lv, 1.5, 0.5,0.5)

# LV geometry (subtract the smallest ellipsoid)
lv = el_lv - el_lv_endo

# Some refinement level
N = 10

# Cut geometry at base
lv_geo = lv - box

# Create mesh
domain = generate_mesh(lv_geo, N)
plot(domain, interactive = True)
