# Copyright (C) 2016 Henrik Finsberg
#
# This file is part of MESH_GENERATION.
#
# MESH_GENERATION is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# MESH_GENERATION is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with MESH_GENERATION. If not, see <http://www.gnu.org/licenses/>.


from mesh_generation.idealized_geometry import idealized, mark_strain_regions
from dolfin import plot, interactive, File

# from mesh_generation.generate_mesh import setup_fiber_parameters
# from mesh_generation.mesh_utils import generate_fibers


def main():

    ellipsoid_types = ["prolate", "ellipsoid", "simple"]
    ndivs = [1,2,3]
    orders = [1,2]
    axisymetrics = [False, True]
    interpolates = [True, False]

 
    # nsectors = [2]
    nsectors = [6,6,4,1]
    
    geo, foc = idealized(ellipsoid_type="simple",
                         ndiv = 1,
                         order = 1,
                         axisymetric = False,
                         interpolate = True,
                         tag = "",
                         nsectors = nsectors)



    # plot(geo.mesh, title = "mesh")
    # plot(geo.sfun, title = "regions")
    # interactive()
    # from IPython import embed; embed()
    # f = File("mesh.pvd")
    # f << geo.mesh
    # from fiberrules import dolfin_to_vtk
    # dolfin_to_vtk(geo.fiber, "fiber2")
    
    # fibparams = setup_fiber_parameters()
    # from IPython import embed; embed()
    exit()
    
    # s = mark_strain_regions(geo.mesh, foc, [2,4], mark_mesh =False)
    # plot(s, title = "some other regions")
    # interactive()

    
if __name__ == "__main__":

    main()
