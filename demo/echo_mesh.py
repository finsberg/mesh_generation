from mesh_generation import *
from logging import DEBUG, INFO
import os
from dolfin import plot, interactive


def setup_paramers():

    h5name = "test.h5"
    echopath = "echopac.h5"

    params = setup_mesh_parameters(echopath, h5name)
  
    # from patient_data.scripts.data import PHASES
    params["time"] = 0#PHASES[name]["passive_filling_begins"]
    params["mesh_type"] = "lv"
    params["strain_type"] = "17"
    # params["strain_type"] = "0"
    params["resolution"] = "low_res"
    # params["patient"] = name

    params["use_gamer"] = False
    params["generate_fibers"] = False
    params["generate_local_basis"] = False
    params["Fibers"]["fiber_space"] = "Quadrature_4"
    params["Fibers"]["include_sheets"] = False

    params["Fibers"]["fiber_angle_epi"] = -60
    params["Fibers"]["fiber_angle_endo"] = 60

    return params
    
def main():

    
    mesh_utils.logger.setLevel(DEBUG)

        
    params = setup_paramers()
    
    M = MeshConstructor(params)
    M.mesh()

    geo = load_geometry_from_h5(params["h5name"], str(params["time"]))


    plot(geo.mesh)
    interactive()

if __name__ == "__main__":
    main()
