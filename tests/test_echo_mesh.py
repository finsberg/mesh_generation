from mesh_generation import *
from logging import DEBUG, INFO
import os
from dolfin import plot, interactive

filepath= os.path.dirname(os.path.abspath(__file__))

strain_types = ["0", "16", "17", "18"]



def setup_paramers():

    h5name = "test.h5"
    echopath = os.path.join(filepath, "../demo/echopac.h5")

    params = setup_mesh_parameters(echopath, h5name)
  
    # from patient_data.scripts.data import PHASES
    params["time"] = 0#PHASES[name]["passive_filling_begins"]
    params["mesh_type"] = "lv"
    params["strain_type"] = "17"
    # params["strain_type"] = "0"
    params["resolution"] = "low_res"
    # params["patient"] = name

    params["use_gamer"] = True
    params["generate_fibers"] = True
    params["generate_local_basis"] = True
    params["Fibers"]["fiber_space"] = "Quadrature_4"
    params["Fibers"]["include_sheets"] = True

    params["Fibers"]["fiber_angle_epi"] = -60
    params["Fibers"]["fiber_angle_endo"] = 60

    return params
    
def test_echo_mesh():
        
    params = setup_paramers()

    
    M = MeshConstructor(params)
    M.mesh()

    geo = load_geometry_from_h5(params["h5name"], str(params["time"]))


if __name__ == "__main__":
    main()
