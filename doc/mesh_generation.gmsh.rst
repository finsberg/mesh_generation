mesh_generation.gmsh package
============================

Submodules
----------

mesh_generation.gmsh.generate_points module
-------------------------------------------

.. automodule:: mesh_generation.gmsh.generate_points
    :members:
    :undoc-members:
    :show-inheritance:

mesh_generation.gmsh.geo2dolfin module
--------------------------------------

.. automodule:: mesh_generation.gmsh.geo2dolfin
    :members:
    :undoc-members:
    :show-inheritance:

mesh_generation.gmsh.gmsh2dolfin module
---------------------------------------

.. automodule:: mesh_generation.gmsh.gmsh2dolfin
    :members:
    :undoc-members:
    :show-inheritance:

mesh_generation.gmsh.gmshfile module
------------------------------------

.. automodule:: mesh_generation.gmsh.gmshfile
    :members:
    :undoc-members:
    :show-inheritance:

mesh_generation.gmsh.inline_backend module
------------------------------------------

.. automodule:: mesh_generation.gmsh.inline_backend
    :members:
    :undoc-members:
    :show-inheritance:

mesh_generation.gmsh.io module
------------------------------

.. automodule:: mesh_generation.gmsh.io
    :members:
    :undoc-members:
    :show-inheritance:

mesh_generation.gmsh.make_affine_mapping module
-----------------------------------------------

.. automodule:: mesh_generation.gmsh.make_affine_mapping
    :members:
    :undoc-members:
    :show-inheritance:

mesh_generation.gmsh.reference_topology module
----------------------------------------------

.. automodule:: mesh_generation.gmsh.reference_topology
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: mesh_generation.gmsh
    :members:
    :undoc-members:
    :show-inheritance:
