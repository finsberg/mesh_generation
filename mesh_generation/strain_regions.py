# Copyright (C) 2016 Henrik Finsberg
#
# This file is part of MESH_GENERATION.
#
# MESH_GENERATION is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# MESH_GENERATION is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with MESH_GENERATION. If not, see <http://www.gnu.org/licenses/>.

"""
 Partitions left ventricle into regions based on strain
 mesh generated from EchoPAC. Genereates local basis
 vector pointing in the radial, cicumferential and
 longitudinal direction
"""
import dolfin
import numpy as np
from mesh_utils import logger

def make_crl_basis_biv(mesh, ffun=None):

    
    from mesh_utils import generate_fibers, get_fiber_markers

    fiber_params ={"fiber_space": "Quadrature_4",
                   "include_sheets": False,
                   "fiber_angle_epi": 0.0,
                   "fiber_angle_endo": 0.0,
                   "sheet_angle_epi": 0.0,
                   "sheet_angle_endo": 0}

    c0 = generate_fibers(mesh, ffun=ffun, **fiber_params)[0]
    c0.rename("circumferential", "local_basis_function")
    
    # Mark mesh for longitudinal
    if ffun is None:
        ffun = dolfin.MeshFunction("size_t", mesh, 2, mesh.domains())
    
    cur_markers = dict((v,k) for k,v in get_fiber_markers("biv").iteritems())
    markers = get_fiber_markers("biv")
    markers["ENDO_RV"] = markers["ENDO_LV"]
    for facet in dolfin.facets(mesh):
        if ffun[facet] != 0:
            mesh.domains().set_marker((facet.index(),
                                       markers[cur_markers[ffun[facet]]]), 2)
    

    
    fiber_params["fiber_angle_epi"]=-90.0
    fiber_params["fiber_angle_endo"]=-90.0

    l0 = generate_fibers(mesh, **fiber_params)[0]
    l0.rename("longitudinal", "local_basis_function")

    for facet in dolfin.facets(mesh):
        mesh.domains().set_marker((facet.index(),ffun[facet]), 2)

    from mesh_generation.strain_regions import calc_cross_products
    r0 = calc_cross_products(c0,l0, c0.function_space())
    r0.rename("radial", "local_basis_function")

    return [c0,r0,l0]
    

def cartesian_to_prolate_ellipsoidal(x,y,z, a):

    b1 = np.sqrt((x + a)**2 + y**2 + z**2)
    b2 = np.sqrt((x - a)**2 + y**2 + z**2)

    sigma = 1/(2.0*a)*(b1 + b2)
    tau = 1/(2.0*a)*(b1 - b2)
    phi = full_arctangent(z, y)
    mu = np.arccosh(sigma)
    nu = np.arccos(tau)
    return mu, nu, phi

def prolate_ellipsoidal_to_cartesian(mu, nu, phi, a):
    x = a*np.cosh(mu)*np.cos(nu)
    y = a*np.sinh(mu)*np.sin(nu)*np.cos(phi)
    z = a*np.sinh(mu)*np.sin(nu)*np.sin(phi)
    return x,y,z




def set_region_borders(strain_regions, foc, strain_type):

    """
    Creates a 16 by 4 marix whith coorinates
    which defines the different regions.

    Example:
    region_sph_coord[0][0] = upper bound for region 1
    region_sph_coord[0][1] = left bound for region 1
    region_sph_coord[9][2] = right bound for region 10
    region_sph_coord[7][3] = lower bound for region 8

    There are 16 regions (neglecting apex)
    For each region there is a start value
    and end value of longitudinal and a start
    value and end value of circumferential.
    So (region,[upper, right, left, lower])
    i.e region_sph_coord(0,0) gives start mu
    for region 1
    """

    region_sph_coord = np.zeros((18, 4)) if strain_type == "18" \
                       else np.zeros((16,4))
    rng = range(18) if strain_type == "18" \
          else range(16)

    # Mid and Basal
    upper = range(4,25, 5)
    left = range(5)
    right = range(20,25)
    lower = range(0,21, 5)

    # Apical
    upper_ap = range(4,35, 5)
    left_ap = range(5)
    right_ap = range(30,35)
    lower_ap = range(0,31, 5)

    for j in rng:

        """
        If 0<=j<=11 then the region are in the
        basal or mid. Then there are in total
        25 nodes for this region. The corner
        points are point number 0 (upper right),
        4 (lower right), 20 (upper left) and
        24 (lower left).
        """

        
        if j in range(12):

            ## upper
            for k in upper:
                p = strain_regions[j][k]
                # Convert to prolate coordinates
                T= cartesian_to_prolate_ellipsoidal(*(p.tolist() +[foc]))
                
                # Divide by 5 to get the average value
                region_sph_coord[j][0] += T[1]/5
                    
            # Left and right
            for side, coords in enumerate([left, right], start = 1):

                arr = []
                for k in coords:
                    p = strain_regions[j][k]
                    T= cartesian_to_prolate_ellipsoidal(*(p.tolist() +[foc]))
                    arr.append(T[2])

                # Because of periodicity we might get values far appart,
                # e.g eps and 2pi - eps
                # Therefore we remove points, until the variace is low
                # so that the mean represents the acutal value. 
                std = np.std(arr)
                while std > 1.0:
                    arr.pop()
                    std = np.std(arr)
                region_sph_coord[j][side] = np.mean(arr)
                
                # Just add pi so that the circumferential direction
                # goes from 0 to 2*pi
                region_sph_coord[j][side] = region_sph_coord[j][side] + np.pi
                
            
            if strain_type == "18" or j in range(0,6):
                # The apical segements are similar to the mid segment
                for k in lower:
                    p = strain_regions[j][k]
                    T= cartesian_to_prolate_ellipsoidal(*(p.tolist() +[foc]))
                    region_sph_coord[j][3] += T[1]/5

            else: #"strain_type" in ["16", "17"]
                # The apical segments span more cells in the
                # circumferential direction than the mid segments.
                # To avoid gaps we need to average the lower coordinates in the
                # mid segments over a larger domain

                if j in range(6,9):
                    for t in range(6,9):
                        for k in range(0,16,5):
                            p = strain_regions[t][k]
                            T= cartesian_to_prolate_ellipsoidal(*(p.tolist() +[foc]))
                            region_sph_coord[j][3] += T[1]/13

                    # Add the last point
                    p = strain_regions[8][20]
                    T= cartesian_to_prolate_ellipsoidal(*(p.tolist() +[foc]))
                    region_sph_coord[j][3] += T[1]/13

                elif j in range(9,12):
                    for t in range(9,12):
                        for k in range(0,16,5):
                            p = strain_regions[t][k]
                            T= cartesian_to_prolate_ellipsoidal(*(p.tolist() +[foc]))
                            region_sph_coord[j][3] += T[1]/13

                    # Add the last point
                    p = strain_regions[11][20]
                    T= cartesian_to_prolate_ellipsoidal(*(p.tolist() +[foc]))
                    region_sph_coord[j][3] += T[1]/13
                else:
                    print("What!!!??")
                
        # Apical segments
        if j >= 12:
            
            # Upper
            if strain_type == "18":

                # Choose the lower bound for the adjacent mid segment
                region_sph_coord[j][0] = region_sph_coord[j-6][3]
               
            else:
                if j in range(12,14):
                    for t in range(12,14):
                        for k in range(4,30,5):
                            p = strain_regions[t][k]
                            T= cartesian_to_prolate_ellipsoidal(*(p.tolist() +[foc]))
                            region_sph_coord[j][0] += T[1]/13
                    # Add the last point
                    p = strain_regions[13][34]
                    T= cartesian_to_prolate_ellipsoidal(*(p.tolist() +[foc]))
                    region_sph_coord[j][0] += T[1]/13

                else: # j in range(14,16)
                    for t in range(14,16):
                        for k in range(4,30,5):
                            p = strain_regions[t][k]
                            T= cartesian_to_prolate_ellipsoidal(*(p.tolist() +[foc]))
                            region_sph_coord[j][0] += T[1]/13
                    # Add the last point
                    p = strain_regions[15][34]
                    T= cartesian_to_prolate_ellipsoidal(*(p.tolist() +[foc]))
                    region_sph_coord[j][0] += T[1]/13

            # Left and right
            for side, coords in enumerate([left_ap, right_ap], start = 1):

                if strain_type == "18":
         
                    # Choose the bound for the adjacent mid segment
                    region_sph_coord[j][side] = region_sph_coord[j-6][side]

                else:
                    arr = []
                    for k in coords:
                        p = strain_regions[j][k]
                        T= cartesian_to_prolate_ellipsoidal(*(p.tolist() +[foc]))
                        arr.append(T[2])

                    # Because of periodicity we might get values far appart,
                    # e.g eps and 2pi - eps
                    # Therefore we remove points, until the variace is low
                    # so that the mean represents the acutal value. 
                    std = np.std(arr)
                    while std > 1.0:
                        arr.pop()
                        std = np.std(arr)
                    region_sph_coord[j][side] = np.mean(arr)

                    # Just add pi so that the circumferential direction
                    # goes from 0 to 2*pi
                    region_sph_coord[j][side] = region_sph_coord[j][side] + np.pi

            for k in lower_ap:
                if strain_type in ["16", "18"]:
                    region_sph_coord[j][3] = 0
                else:
                    p = strain_regions[j][k]
                    T= cartesian_to_prolate_ellipsoidal(*(p.tolist() +[foc]))
                    region_sph_coord[j][3] += T[1]/7
        
    return region_sph_coord

def get_sector(regions, theta):

    if not(np.count_nonzero(regions.T[1] <= regions.T[2]) >= 0.5*np.shape(regions)[0]):
        raise ValueError, "Surfaces are flipped"
        
    sectors = []
    for i, r in enumerate(regions):

        if r[1] == r[2]:
            sectors.append(i)
        else:
                     
            if r[1] > r[2]:
                if theta > r[1] or r[2] > theta:
                    sectors.append(i)

            else:
                if r[1] < theta < r[2]:
                    sectors.append(i)
        

    return sectors


def get_level(regions, mu):

    levels = []

    A = np.intersect1d(np.where((regions.T[3] <= mu))[0],
                       np.where((mu<= regions.T[0]))[0])
    if len(A) == 0:
        return [np.shape(regions)[0] +1]
    else:
        return  A


def strain_region_number(T, regions):
    """
    For a given point in prolate coordinates,
    return the region it belongs to.

    :param regions: Array of all coordinates for the strain 
                    regions taken from the strain mesh.
    :type regions: :py:class:`numpy.array`

    :param T: Some value i prolate coordinates
    :type T: :py:class:`numpy.array`

    Resturn the region number that
    T belongs to
    """

    """
    The cricumferential direction is a bit
    tricky because it goes from -pi to pi.
    To overcome this we add pi so that the
    direction goes from 0 to 2*pi
    """

    lam, mu, theta = T

    theta = theta + np.pi

    levels = get_level(regions, mu)

    if np.shape(regions)[0] +1 in levels:
       return np.shape(regions)[0] +1

    sector = get_sector(regions, theta)

    try:
        assert len(np.intersect1d(levels, sector)) == 1
    except:
        from IPython import embed; embed()
        exit()

    return np.intersect1d(levels, sector)[0] + 1


def mark_cell_function(fun, mesh, foc, regions):
    """
    Iterates over the mesh and stores the
    region number in a meshfunction
    """

    
    for cell in dolfin.cells(mesh):

        # Get coordinates to cell midpoint
        x = cell.midpoint().x()
        y = cell.midpoint().y()
        z = cell.midpoint().z()

        # Get index of cell
        index = cell.index()

        T= cartesian_to_prolate_ellipsoidal(x,y,z,foc)

        fun[cell] = strain_region_number(T, regions)

    return fun

def mark_lv_strain_regions(fun, mesh, focal_point,
                           strain_regions, strain_type):
    """
    Takes in a dolfin meshfunction (fun) and stores the
    region number (1-17) for each cell in that mesh function
    """

    regions = set_region_borders(strain_regions, focal_point, strain_type)

    # Fix the bounds on the regions so that each point belongs
    # to a unique segment
    base_rng = range(6)
    BASE_LOW = np.mean([regions[i][3] for i in base_rng])
    BASE_MAX = np.inf#np.max([regions[i][0] for i in base_rng]) + 1.0
    regions[base_rng, 3] = BASE_LOW
    regions[base_rng, 0] = BASE_MAX

    mid_rng = range(6,12)
    MID_LOW = np.mean([regions[i][3] for i in mid_rng])
    regions[mid_rng, 3] = MID_LOW
    regions[mid_rng, 0] = BASE_LOW
    
    ap_rng = range(12,16) if np.shape(regions)[0] < 18 else range(12,18)        
    APICAL_LOW = np.mean([regions[i][3] for i in ap_rng])
    regions[ap_rng, 3] = APICAL_LOW
    regions[ap_rng, 0] = MID_LOW


    fun = mark_cell_function(fun, mesh, focal_point, regions)

    return fun


def full_arctangent(x,y):
    t = np.arctan2(x, y)
    if t < 0:
        return t + 2*np.pi
    else:
        return t


def fill_coordinates_ec(i, e_c_x, e_c_y, e_c_z, coord, foci):
    norm = dolfin.sqrt(coord[1]**2 + coord[2]**2)
    if not dolfin.near(norm, 0):
        e_c_y.vector()[i] = -coord[2]/norm
        e_c_z.vector()[i] = coord[1]/norm
    else:
        #We are at the apex where clr system doesn't make sense
        #So just pick something.
        e_c_y.vector()[i] = 1
        e_c_z.vector()[i] = 0

def fill_coordinates_el(i, e_c_x, e_c_y, e_c_z, coord, foci):

    norm = dolfin.sqrt(coord[1]**2 + coord[2]**2)
    if not dolfin.near(norm, 0):
        mu,nu,phi = cartesian_to_prolate_ellipsoidal(*(coord.tolist() +[foci]))
        x,y,z = prolate_ellipsoidal_to_cartesian(mu, nu + 0.01, phi, foci)
        r = np.array([coord[0] - x,
                      coord[1] - y,
                      coord[2] - z])
        e_r = r/np.linalg.norm(r)
        e_c_x.vector()[i] = e_r[0]
        e_c_y.vector()[i] = e_r[1]
        e_c_z.vector()[i] = e_r[2]
    else:
        e_c_y.vector()[i] = 0
        e_c_z.vector()[i] = 1


def calc_cross_products(e1, e2, VV):
    e_crossed = dolfin.Function(VV)

    e1_arr = e1.vector().array().reshape((-1, 3))
    e2_arr = e2.vector().array().reshape((-1, 3))

    crosses = []
    for c1, c2 in zip(e1_arr, e2_arr):
        crosses.extend(np.cross(c1, c2.tolist()))

    e_crossed.vector()[:] = np.array(crosses)[:]
    return e_crossed


def check_norms(e):

    e_arr = e.vector().array().reshape((-1, 3))
    for e_i in e_arr:
        assert(near(np.linalg.norm(e_i), 1.0))

def make_unit_vector(V, VV, dofs_x, fill_coordinates, foc = None):
    e_c_x = dolfin.Function(V)
    e_c_y = dolfin.Function(V)
    e_c_z = dolfin.Function(V)

    for i,coord in enumerate(dofs_x):
        fill_coordinates(i, e_c_x, e_c_y, e_c_z, coord, foc)

    e = dolfin.Function(VV)

    fa = [dolfin.FunctionAssigner(VV.sub(i), V) for i in range(3)]
    for i, e_c_comp in enumerate([e_c_x, e_c_y, e_c_z]):
        fa[i].assign(e.split()[i], e_c_comp)
    return e

def make_crl_basis(mesh, foc, space = "Quadrature_4"):
    """
    Makes the crl  basis for the idealized mesh (prolate ellipsoidal)
    with prespecified focal length.
    """

    msg = ("Creating local basis function in the "+
           "circumferential, radial and longitudinal "+
           "direction...")
    logger.info(msg)
    family, degree = space.split("_")
    fem = dolfin.FiniteElement(family = family,
                               cell = mesh.ufl_cell(),
                               degree = int(degree),
                               quad_scheme="default")
    vem = dolfin.VectorElement(family = family,
                               cell = mesh.ufl_cell(),
                               degree = int(degree),
                               quad_scheme="default")
    
    VV = dolfin.FunctionSpace(mesh, vem)
    V = dolfin.FunctionSpace(mesh, fem)

    if dolfin.DOLFIN_VERSION_MAJOR > 1.6:
        dofs_x = V.tabulate_dof_coordinates().reshape((-1, mesh.geometry().dim()))
    else:
        dm = V.dofmap()
        dofs_x = dm.tabulate_all_coordinates(mesh).reshape((-1, mesh.geometry().dim()))

    logger.info("Creating circumferential")
    e_c = make_unit_vector(V, VV, dofs_x, fill_coordinates_ec)
    logger.info("Done creating circumferential")
    logger.info("Creating longitudinal")
    e_l = make_unit_vector(V, VV, dofs_x, fill_coordinates_el, foc)
    logger.info("Done creating longitudinal")
    logger.info("Creating radial")
    e_r = calc_cross_products(e_c, e_l, VV)
    logger.info("Done creating radial")

    e_c.rename("circumferential", "local_basis_function")
    e_r.rename("radial", "local_basis_function")
    e_l.rename("longitudinal", "local_basis_function")

    return e_c, e_r, e_l
