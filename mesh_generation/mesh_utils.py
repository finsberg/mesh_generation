# Copyright (C) 2016 Henrik Finsberg
#
# This file is part of MESH_GENERATION.
#
# MESH_GENERATION is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# MESH_GENERATION is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with MESH_GENERATION. If not, see <http://www.gnu.org/licenses/>.
# from dolfin import *
import dolfin

import numpy as np
import strain_regions as sr

import os, yaml

try:
    import h5py
    has_h5py = True
except:
    print "Warning: h5py is not installed"
    has_h5py = False

parallel_h5py = h5py.h5.get_config().mpi

try:
    import mpi4py, petsc4py
    has_mpi4py = True
except:
    has_mpi4py = False
    if parallel_h5py: raise ImportError
else:
    from mpi4py import MPI as mpi4py_MPI

ROUND_OFF_FILE =  "round_off.yml"


# Mesh should be in cm
SCALE = 100


ESTIMATE_FOCAL_POINT = False
DEFAULT_FOCAL_POINT = 6.0

# Logger
import logging
log_level = logging.INFO
def make_logger(name, level = logging.INFO):

    mpi_filt = lambda: None
    def log_if_proc0(record):
        if dolfin.MPI.rank(dolfin.mpi_comm_world()) == 0:
            return 1
        else:
            return 0
        
    mpi_filt.filter = log_if_proc0

    logger = logging.getLogger(name)
    logger.setLevel(log_level)

    ch = logging.StreamHandler()
    ch.setLevel(0)


    formatter = logging.Formatter('%(message)s')
    ch.setFormatter(formatter)
    

    logger.addHandler(ch)
    logger.addFilter(mpi_filt)

    
    dolfin.set_log_active(False)
    dolfin.set_log_level(dolfin.WARNING)
    
    return logger

logger = make_logger("Mesh generation", log_level)

### Get/Load stuff ####
def open_h5py(h5name, file_mode="a", comm= dolfin.mpi_comm_world()):
    
    if has_mpi4py:
        assert isinstance(comm, (petsc4py.PETSc.Comm, mpi4py.MPI.Intracomm))
        
    if parallel_h5py:
        if isinstance(comm, petsc4py.PETSc.Comm):
            comm = comm.tompi4py()
        
        return  h5py.File(h5name, file_mode, driver='mpio', comm=comm)
    else:
        return  h5py.File(h5name, file_mode)

def check_h5group(h5name, h5group, delete = False, comm= dolfin.mpi_comm_world()):

    h5group_in_h5file = False
    if not os.path.isfile(h5name): return False

    filemode = "a" if delete else "r"
    if not os.access(h5name, os.W_OK):
        filemode = "r"
        if delete:
            logger.warning("You do not have write access to file {}".format(h5name))
            delete =False
    
    with open_h5py(h5name, filemode, comm) as h5file:
        if h5group in h5file:
            h5group_in_h5file = True
            if delete:
                if parallel_h5py:
                
                    logger.debug("Deleting existing group: '{}'".format(h5group))
                    del h5file[h5group]

                else:
                    if dolfin.MPI.rank(comm) == 0:
                        
                        logger.debug("Deleting existing group: '{}'".format(h5group))
                        del h5file[h5group]
                        
    return h5group_in_h5file
        
        


def load_echo_geometry(echo_path, time, scale = 100):
    """Get geometric data from echo data. 
    Get verices and faces of the endo- and epicardium,
    as well as a strain mesh defining the location of the
    AHA segements

    :param str echo_path: Path to the echo file
    :param int time: Which time stamp
    :param float scale: Scale of the geometry.
    The original surfaces is in m3, so cm = 100, mm = 1000              
    :returns: A dictionary with the data
    :rtype: dict

    """
    
    import surface as surf
    assert os.path.isfile(echo_path), "File {} does not exist".format(echo_path)

    data = {}
    with h5py.File(echo_path, "r") as echo_file:
        
        epi = echo_file['/LV_Mass_Epi']
        endo = echo_file['/LV_Mass_Endo']

        data["epi_faces"] = np.array(epi['indices'])
        # Take out epi vertices for the timeslot given
        # and convert from m3 to cm3
        data["epi_verts"] = scale*np.array(epi['vertices'])[time,:,:]
            
        data["endo_faces"] = np.array(endo['indices'])
        # Take out endo vertices for the timeslot given
        # and convert from m3 to cm3
        data["endo_verts"] = scale*np.array(endo['vertices'])[time,:,:]

        # Strain mesh containing information about AHA segment
        # Convert from m3 to cm3
        data["strain_mesh"] = scale*np.array(echo_file['/LV_Strain/mesh'])[time,:,:]


    data["strain_coords"] = surf.get_strain_region_coordinates(data["strain_mesh"])

    return data   

def create_lv_ply_files(echo_path, time, round_off = 0.0,
                        esitmate_focal_point = True, plydir = None,
                        use_gamer = False, cut_base = True):
    """Create ply files to be used for lv mesh generation

    :param patient: Name of patient
    :param time: the timestamp (index) from echo
    :param round_off: Value determining the location of the basal plane
    :param esitmate_focal_point: (bool) Estimate focal point or use default (6.0)
    :param plydir: directory for the plyfiles. If None a temporary directory is created
    :param bool use_gamer: Use gamer to smooth surfaces before meshing
    :returns: the surfaces ready for meshing
    :rtype: dict

    """
    import surface as surf

    original_data = load_echo_geometry(echo_path, time)
    if plydir is not None:
        surf.save_surfaces_to_ply(plydir, time,  original_data, "_raw")
        
        
    # Original long axis diameter
    from scipy.spatial import distance
    long_axis_endo = np.max(distance.cdist(original_data["endo_verts"],
                                           original_data["endo_verts"],
                                           "euclidean"))

    # Transform the data
    transformed_data = surf.transform_surfaces(round_off, **original_data)


    # Cut of the base
    if cut_base:
        data = surf.remove_base(**transformed_data)
    else:
        data = transformed_data

    if use_gamer:
        data = surf.smooth_surface_gamer(data)
    
    if esitmate_focal_point:
        foc = surf.compute_focal_point(long_axis_endo, **data)
    else:
        foc = DEFAULT_FOCAL_POINT

    data["focal_point"] = foc

    if plydir is None:
        import tempfile
        plydir = tempfile.mkdtemp()
        
    
    surf.save_surfaces_to_ply(plydir, time,  data)
    data["plydir"] = plydir
    return data
    
def get_time_stamps(h5path):
    """
    Get the time stamps for the
    echo data.

    :param str h5path: Path to the file containg echo data
    :returns: list of time stamps
    :rtype: list

    """
    assert os.path.isfile(h5path), "Invalid name {}".format(h5path)
    echo_file = h5py.File(h5path, "r")
    time_stamps = np.array(echo_file['time_stamps'])
    echo_file.close()
    return time_stamps

def get_markers(mesh_type = "lv"):
    
    assert mesh_type in ["lv", "biv"]

    fiber_markers = get_fiber_markers(mesh_type)

    markers = {}
    markers["NONE"] = (0, 3) 

    markers["BASE"] = (fiber_markers["BASE"], 2) 
    markers["EPI"] =  (fiber_markers["EPI"], 2)
    markers["EPIRING"] =  (fiber_markers["EPIRING"], 1)

    if mesh_type == "lv":
    
        markers["ENDO"] = (fiber_markers["ENDO"], 2)
        
        markers["ENDORING"] =  (fiber_markers["ENDORING"], 1)

    else:

        markers["ENDO_RV"] = (fiber_markers["ENDO_RV"], 2)
        markers["ENDO_LV"] = (fiber_markers["ENDO_LV"], 2)
        
        markers["ENDORING_RV"] =  (fiber_markers["ENDORING_RV"], 1)
        markers["ENDORING_LV"] =  (fiber_markers["ENDORING_LV"], 1)

    return markers


def get_fiber_markers(mesh_type = "lv"):
    """
    Get the markers for the mesh.
    This is the default markers for fiberrules.

    :param str mesh_type: type of mesh, 'lv' or 'biv'
    :returns: The markers 
    :rtype: dict

    """
    
    if mesh_type == "lv":
        
        return {'BASE':10,  'ENDO': 30, 'EPI': 40, 
                'WALL': 50, 'ENDORING': 300, 
                'EPIRING': 400, 'WALL':50}

    elif mesh_type == "biv":
        
        return {'BASE':10, 'ENDO_RV': 20, 'ENDO_LV': 30, 'EPI': 40, \
                'ENDORING_RV': 200, 'ENDORING_LV': 300, \
                'EPIRING': 400, 'WALL':50}
                
def load_geometry_from_h5(h5name, h5group = "",
                          fendo = None, fepi = None,
                          include_sheets = False,
                          comm = dolfin.mpi_comm_world()):
    """Load geometry and other mesh data from
    a h5file to an object.
    If the file contains muliple fiber fields 
    you can spefify the angles, and if the file
    contais sheets and cross-sheets this can also
    be included

    :param str h5name: Name of the h5file
    :param str h5group: The group within the file
    :param int fendo: Helix fiber angle (endocardium) (if available)
    :param int fepi: Helix fiber angle (epicardium) (if available)
    :param bool include_sheets: Include sheets and cross-sheets 
    :returns: An object with geometry data
    :rtype: object

    """
    

    logger.info("\nLoad mesh from h5")
    # Set default groups
    ggroup = '{}/geometry'.format(h5group)
    mgroup = '{}/mesh'.format(ggroup)
    lgroup = "{}/local basis functions".format(h5group)
    fgroup = "{}/microstructure/".format(h5group)

    if not os.path.isfile(h5name):
        raise IOError("File {} does not exist".format(h5name))

    # Check that the given file contains
    # the geometry in the given h5group
    if not check_h5group(h5name, mgroup, delete = False, comm = comm):
        msg = "Warning!\nGroup: '{}' does not exist in file:\n{}".format(mgroup, h5name)
        with h5py.File(h5name) as h: keys = h.keys()
        msg += "\nPossible values for the h5group are {}".format(keys)
        raise IOError(msg)
        
    
    # Create a dummy object for easy parsing
    class Geometry(object): pass
    geo = Geometry()
    
    with dolfin.HDF5File(comm, h5name, "r") as h5file:
        
        # Load mesh        
        mesh = dolfin.Mesh(comm)
        # h5file.read(mesh, mgroup, False)
        h5file.read(mesh, mgroup, True)
        geo.mesh = mesh
        
        # Get mesh functions
        for dim, attr in zip(range(4), ["vfun", "rfun", "ffun", "sfun"]):

            dgroup = '{}/mesh/meshfunction_{}'.format(ggroup, dim)
            mf = dolfin.MeshFunction("size_t", mesh, dim, mesh.domains())
            if h5file.has_dataset(dgroup):
                h5file.read(mf, dgroup)

            setattr(geo, attr, mf)
            

        if h5file.has_dataset(lgroup):
            # Get local bais functions
            local_basis_attrs = h5file.attributes(lgroup)
            lspace = local_basis_attrs["space"]
            family, order = lspace.split('_')

            namesstr = local_basis_attrs["names"]
            names = namesstr.split(":")

            if dolfin.DOLFIN_VERSION_MAJOR > 1.6:
                elm = dolfin.VectorElement(family = family,
                                           cell = mesh.ufl_cell(),
                                           degree = int(order),
                                           quad_scheme="default")
                V = dolfin.FunctionSpace(mesh, elm)
            else:
                V = dolfin.VectorFunctionSpace(mesh, family, int(order))
        
            for name in names:
                l = dolfin.Function(V, name = name)
                h5file.read(l, lgroup+"/{}".format(name))
                setattr(geo, name, l)
        
        if h5file.has_dataset(fgroup):
            # Get fibers
            fiber_attrs = h5file.attributes(fgroup)
            fspace = fiber_attrs["space"]
            if fspace is None:
                #Assume quadrature 4
                family = "Quadrature"
                order = 4
            else:
                family, order = fspace.split('_')
        
            namesstr = fiber_attrs["names"]
            if namesstr is None:
                names_ = ["fiber"]
            else:
                names_ = namesstr.split(":")

                
            if (fepi and fendo) is None:
                names = names_
                # fepi, fendo = np.array(names_[0].split("fiber_epi")[1].split("_endo"), dtype = int)
                
            else:

                

                if not include_sheets or len(names_) < 3:
                    names = ["fiber_epi{}_endo{}".format(fepi, fendo)]
                    
                else:
                    names = ["fiber_epi{}_endo{}".format(fepi, fendo),
                             names_[1], "_".join(names_[2].split("_")[:2] \
                                                 + ["fepi{}_fendo{}".format(fepi, fendo)] \
                                                 + names_[2].split("_")[-2:])]
            
           
            # Check that these fibers exists
            for name in names:
                fsubgroup = fgroup+"/{}".format(name)
                if not h5file.has_dataset(fsubgroup):
                    names = names_
                    msg = "Warning: Given fiber sheet system does not exist\n" \
                          +"{} is missing".format(fsubgroup) \
                          +"{} will be used in stead".format(names)
                    logger.warning(msg)
                    # fepi, fendo = np.array(names_[0].split("fiber_epi")[1].split("_endo"), dtype = int)

            # geo.fiber_angle_epi = fepi
            # geo.fiber_angle_endo = fendo

            if dolfin.DOLFIN_VERSION_MAJOR > 1.6:
                elm = dolfin.VectorElement(family = family,
                                           cell = mesh.ufl_cell(),
                                           degree = int(order),
                                           quad_scheme="default")
                V = dolfin.FunctionSpace(mesh, elm)
            else:
                V = dolfin.VectorFunctionSpace(mesh, family, int(order))
                                                    
                                                      
            attrs = ["fiber", "sheet", "sheet_normal"] 
            for i, name in enumerate(names):
                l = dolfin.Function(V, name = name)
                fsubgroup = fgroup+"/{}".format(name)
                h5file.read(l, fsubgroup)
                    
                fsub_attrs = h5file.attributes(fsubgroup)
                # setattr(geo, fsub_attrs["name"], l)
                setattr(geo, attrs[i], l)

        # Load the boundary markers
        try:
            markers = {}
            for dim in range(mesh.ufl_domain().topological_dimension()+1):
                for key_str in ["domain", "meshfunction"]:
                    dgroup = '{}/mesh/{}_{}'.format(ggroup, key_str, dim)

                    # If dataset is not present
                    if not h5file.has_dataset(dgroup):
                        continue

                    for aname in h5file.attributes(dgroup).str().strip().split(' '):
                        if aname.startswith('marker_name'): 

                            name =  aname.rsplit('marker_name_')[-1]
                            marker = h5file.attributes(dgroup)['marker_name_{}'.format(name)]
                            markers[name] = (int(marker), dim)
        except:
            markers = get_markers()


        origmeshgroup="{}/original_geometry".format(h5group)
        if h5file.has_dataset(origmeshgroup):
            original_mesh = dolfin.Mesh(comm)
            h5file.read(original_mesh, origmeshgroup, True)
            setattr(geo, "original_geometry", original_mesh)
        

        geo.markers = markers

    
        
    return geo

def load_fibers(h5name, h5group, mesh, fiber_str):
    """Load fibers (only) from the geometry file

    :param str h5name: 
    :param str h5group: 
    :param mesh: The underlying mesh
    :type mesh: (:py:class:`dolfin.Mesh`)
    :param str fiber_str: Name of the fiber group
    :returns: The fiber field
    :rtype: (:py:class:`dolfin.Function`)

    """
    
    
    fgroup = "{}/microstructure/".format(h5group)
    with dolfin.HDF5File(dolfin.mpi_comm_world(), h5name, "r") as h5file:
        
        # Get fibers
        fiber_attrs = h5file.attributes(fgroup)
        fspace = fiber_attrs["space"]
        family, order = fspace.split('_')
        
        namesstr = fiber_attrs["names"]
        names = namesstr.split(":")
        V = dolfin.VectorFunctionSpace(mesh, family, int(order))

        l = dolfin.Function(V, name = fiber_str)
        fsubgroup = fgroup+"/{}".format(fiber_str)
        h5file.read(l, fsubgroup)

        return l
    

    
def get_round_off_buffer(patient, time):
    if os.path.isfile(ROUND_OFF_FILE):
        with open(ROUND_OFF_FILE, 'r') as outfile:
            dic = yaml.load(outfile)

        try:
            return float(dic[patient][str(time)])
        except:
            return False
    else:
        return False
def get_measured_volume(echo_path, time):

    h5file = h5py.File(echo_path, "r")
    volumes = np.array(h5file["LV_Volume_Trace"])*1000*1000
    vol  = volumes[time]
    h5file.close()

    return vol

### Create/generate stuff ###
def generate_local_basis_functions(mesh, focal_point):
    """Generate vector field for the circumferential, 
    radial and longitudinal direction.

    :param mesh: The mesh
    :type mesh: (:py:class:`dolfin.Mesh`)
    :param float focal_point: Focel point
    :returns: List of functions 
    [circumeferential, Radial, Longitudinal]
    :rtype: list

    """

    # Make basis functions
    c, r, l = sr.make_crl_basis(mesh, focal_point) 

    return [c,r,l]
    

def generate_strain_markers(mesh, focal_point,
                            strain_regions, strain_type):
    """Generate markers for the AHA segements, 
    and mark the mesh accordingly

    :param mesh: The mesh
    :type mesh: (:py:class:`dolfin.Mesh`)
    :param float focal_point: Focal point
    :param strain_regions: 
    :param str strain_type: 16 or 17 segements


    """
    

    
    # Strain Markers
    sfun = dolfin.MeshFunction("size_t", mesh, 3)
    sfun = sr.mark_lv_strain_regions(sfun,
                                     mesh,
                                     focal_point,
                                     strain_regions,
                                     strain_type)


    
    # Mark the cells accordingly
    for cell in dolfin.cells(mesh):
        mesh.domains().set_marker((cell.index(), sfun[cell]), 3)



def generate_fibers(mesh, fiber_angle_endo=60, fiber_angle_epi=-60,
                    sheet_angle_endo=0, sheet_angle_epi=0, include_sheets=True,
                    fiber_space="Quadrature_4", ffun=None):
    """
    Generate fiber architecture using the Bayer 
    algorithm [1]. This algorithm is implemented
    in the package fiberrules. Contact Johan Hake
    for access. 

    :param mesh: The mesh
    :type mesh: :py:class`dolfin.Mesh`
    :param dict fiber_params: Parameter for fibers
    :returns: List for fiber fields (and possible sheets and 
              fiber-sheet normals)
    :rtype: list


    .. rubric:: Reference

    [1] Bayer, J. D., et al. "A novel rule-based algorithm 
    for assigning myocardial fiber orientation to 
    computational heart models." Annals of biomedical 
    engineering 40.10 (2012): 2243-2254.
    
    """
    

    from fiberrules import dolfin_fiberrules, dolfin_to_vtk

    logger.info("\nGENERATING FIBERS ...")



    assert len(fiber_space.split("_"))==2, \
               "expected fiber_space_name in 'FamilyName_Degree' format"
    
    family, degree = fiber_space.split("_")

    if dolfin.DOLFIN_VERSION_MAJOR > 1.6:
        fiber_space_ = dolfin.FunctionSpace(mesh, dolfin.FiniteElement(family=family, cell=mesh.ufl_cell(),
               degree =int(degree), quad_scheme = "default"))
    else:
        fiber_space_ = dolfin.FunctionSpace(mesh, family, int(degree))

    if family == "Quadrature":
        dolfin.parameters["form_compiler"]["quadrature_degree"] = int(degree)

    # There are some strange shifting in the fiberrults angles
    # fiber_angle_epi = 90 - (-fiber_params["fiber_angle_epi"]) #- 90 #-180
    # fiber_angle_endo = 90 - (fiber_params["fiber_angle_endo"]) #+ 270 #- 180
    # # fiber_angle_epi = -fiber_params["fiber_angle_epi"] - 90 #-180
    # # fiber_angle_endo = fiber_params["fiber_angle_endo"] + 270 #- 180
    # sheet_angle_endo = fiber_params["sheet_angle_endo"]
    # sheet_angle_epi = fiber_params["sheet_angle_epi"]

    # There are some strange shifting in the fiberrults angles
    fiber_angle_epi_ = 90 - (-fiber_angle_epi) #- 90 #-180
    fiber_angle_endo_ = 90 - (fiber_angle_endo) #+ 270 #- 180
    # fiber_angle_epi = -fiber_params["fiber_angle_epi"] - 90 #-180
    # fiber_angle_endo = fiber_params["fiber_angle_endo"] + 270 #- 180
    sheet_angle_endo_ = sheet_angle_endo
    sheet_angle_epi_ = sheet_angle_epi

    if ffun is not None:

        for facet in dolfin.facets(mesh):
            mesh.domains().set_marker((facet.index(), ffun[facet]), 2)

    microstructures = dolfin_fiberrules(mesh, 
                                        fiber_space_,
                                        fiber_angle_epi_, 
                                        fiber_angle_endo_,
                                        sheet_angle_epi_,
                                        sheet_angle_endo_)


    microstructures[0].rename("fiber", "epi{}_endo{}".format(fiber_angle_epi, 
                                                             fiber_angle_endo))
    fields = [microstructures[0]]


    if include_sheets:
        microstructures[1].rename("sheet", "epi{}_endo{}".format(sheet_angle_epi, 
                                                                sheet_angle_endo))
        microstructures[2].rename("cross_sheet", \
                                 "fepi{}_fendo{}_sepi{}_sendo{}".format(fiber_angle_epi, 
                                                                        fiber_angle_endo,
                                                                        sheet_angle_epi, 
                                                                        sheet_angle_endo))
        fields.append(microstructures[1])
        fields.append(microstructures[2])

    return fields


def create_geometry_with_strain(meshdir, case, stage, mesh_scaling=1.0, markings = None) :
    """
    Create geometry by meshing the three surfaces (endo lv, endo rv and epi)
    together using a wrapped version for gmsh.
    This also mesh the strain patches inside the mesh.

    NOTE: The strain mesh will in some cases intersect the endo lv surface.
    In this case we are not able to mesh the patches inside the mesh.
    """
    from textwrap import dedent
    geocode = dedent(\
    """\
    // meshing options
    Mesh.CharacteristicLengthFromCurvature = 1;
    Mesh.Lloyd = 1;
    Mesh.CharacteristicLengthMin = 0.8;
    Mesh.CharacteristicLengthMax = 0.8;
    Mesh.Optimize = 1;
    Mesh.OptimizeNetgen = 1;
    Mesh.RemeshParametrization = 7;
    Mesh.SurfaceFaces = 1;
    Mesh.CharacteristicLengthFactor = {mesh_scaling};

    // load the surfaces
    Merge "{meshdir}/endo_lv_us_{stage}.ply";
    Merge "{meshdir}/endo_rv_us_{stage}.ply";
    Merge "{meshdir}/epi_us_{stage}.ply";

    // load strain pathes
    Merge "{meshdir}/strain_region1_{stage}.ply";
    Merge "{meshdir}/strain_region2_{stage}.ply";
    Merge "{meshdir}/strain_region3_{stage}.ply";
    Merge "{meshdir}/strain_region4_{stage}.ply";
    Merge "{meshdir}/strain_region5_{stage}.ply";
    Merge "{meshdir}/strain_region6_{stage}.ply";
    Merge "{meshdir}/strain_region7_{stage}.ply";
    Merge "{meshdir}/strain_region8_{stage}.ply";
    Merge "{meshdir}/strain_region9_{stage}.ply";
    Merge "{meshdir}/strain_region10_{stage}.ply";
    Merge "{meshdir}/strain_region11_{stage}.ply";
    Merge "{meshdir}/strain_region12_{stage}.ply";
    Merge "{meshdir}/strain_region13_{stage}.ply";
    Merge "{meshdir}/strain_region14_{stage}.ply";
    Merge "{meshdir}/strain_region15_{stage}.ply";
    Merge "{meshdir}/strain_region16_{stage}.ply";
    Merge "{meshdir}/strain_region17_{stage}.ply";

    CreateTopology;

    ll[] = Line "*";
    L_LV_base = newl; Compound Line(L_LV_base) = ll[2];
    L_RV_base = newl; Compound Line(L_RV_base) = ll[0];
    L_epi_base = newl; Compound Line(L_epi_base) = ll[1];
    Physical Line("ENDORING_LV") = {{ L_LV_base }};
    Physical Line("ENDORING_RV") = {{ L_RV_base }};
    Physical Line("EPIRING") = {{ L_epi_base }};

    L_R1_base = newl; Compound Line(L_R1_base) = ll[16];
    L_R2_base = newl; Compound Line(L_R2_base) = ll[12];
    L_R3_base = newl; Compound Line(L_R3_base) = ll[20];
    L_R4_base = newl; Compound Line(L_R4_base) = ll[24];
    L_R5_base = newl; Compound Line(L_R5_base) = ll[8];
    L_R6_base = newl; Compound Line(L_R6_base) = ll[7];

    L_strain = newl; 
    Compound Line(L_strain) = {{ L_R1_base, L_R2_base, L_R3_base, L_R4_base, L_R5_base, L_R6_base }};


    ss[] = Surface "*";
    S_LV = news; Compound Surface(S_LV) = ss[0];
    S_RV = news; Compound Surface(S_RV) = ss[1];
    S_epi = news; Compound Surface(S_epi) = ss[2];

    S_1 = news; Compound Surface(S_1) = ss[3];		
    S_2 = news; Compound Surface(S_2) = ss[4];
    S_3 = news; Compound Surface(S_3) = ss[5];
    S_4 = news; Compound Surface(S_4) = ss[6];
    S_5 = news; Compound Surface(S_5) = ss[7];
    S_6 = news; Compound Surface(S_6) = ss[8];
    S_7 = news; Compound Surface(S_7) = ss[9];
    S_8 = news; Compound Surface(S_8) = ss[10];
    S_9 = news; Compound Surface(S_9) = ss[11];
    S_10 = news; Compound Surface(S_10) = ss[12];
    S_11 = news; Compound Surface(S_11) = ss[13];
    S_12 = news; Compound Surface(S_12) = ss[14];
    S_13 = news; Compound Surface(S_13) = ss[15];
    S_14 = news; Compound Surface(S_14) = ss[16];
    S_15 = news; Compound Surface(S_15) = ss[17];
    S_16 = news; Compound Surface(S_16) = ss[18];
    S_17 = news; Compound Surface(S_17) = ss[19];

    S_strain = news; Compound Surface(S_strain) = {{ S_1, S_2, S_3, S_4, S_5, S_6, S_7, S_8, S_9, S_10, S_11, S_12, S_13, S_14, S_15, S_16, S_17 }};

    Physical Surface("ENDO_LV") = {{ S_LV }};
    Physical Surface("ENDO_RV") = {{ S_RV }};
    Physical Surface("EPI") = {{ S_epi }};
    Physical Surface("REGION_1") = {{ S_1 }};
    Physical Surface("REGION_2") = {{ S_2 }};
    Physical Surface("REGION_3") = {{ S_3 }};
    Physical Surface("REGION_4") = {{ S_4 }};
    Physical Surface("REGION_5") = {{ S_5 }};
    Physical Surface("REGION_6") = {{ S_6 }};
    Physical Surface("REGION_7") = {{ S_7 }};
    Physical Surface("REGION_8") = {{ S_8 }};
    Physical Surface("REGION_9") = {{ S_9 }};
    Physical Surface("REGION_10") = {{ S_10 }};
    Physical Surface("REGION_11") = {{ S_11 }};
    Physical Surface("REGION_12") = {{ S_12 }};
    Physical Surface("REGION_13") = {{ S_13 }};
    Physical Surface("REGION_14") = {{ S_14 }};
    Physical Surface("REGION_15") = {{ S_15 }};
    Physical Surface("REGION_16") = {{ S_16 }};
    Physical Surface("REGION_17") = {{ S_17 }};


    LL_base_1 = newll; 
    Line Loop(LL_base_1) = {{ L_LV_base,L_R1_base, -L_R2_base, -L_R3_base, -L_R4_base, -L_R5_base, L_RV_base, L_epi_base, L_R6_base }};
    S_base_1 = news; Plane Surface(S_base_1) = {{ LL_base_1 }};

    LL_base_2 = newll;
    Line Loop(LL_base_2) = {{ L_R1_base, -L_R2_base, -L_R3_base, -L_R4_base, -L_R5_base,L_RV_base, -L_R6_base }};
    S_base_2 = news; Plane Surface(S_base_2) = {{ LL_base_2 }};

    Physical Surface("BASE") = {{ S_base_1, S_base_2 }};

    SL_wall1 = newsl; 
    Surface Loop(SL_wall1) = {{ S_LV, S_strain, S_base_2 }};

    SL_wall2 = newsl; 
    Surface Loop(SL_wall2) = {{ S_strain, S_RV, S_epi, S_base_1 }};


    V_wall1 = newv; Volume(V_wall1) = {{ SL_wall1 }};
    V_wall2 = newv; Volume(V_wall2) = {{ SL_wall2 }};
    Physical Volume("WALL") = {{ V_wall1, V_wall2 }};

    """.format(stage=stage, case=case, meshdir=meshdir,
                mesh_scaling=mesh_scaling))

    from gmsh import geo2dolfin
    mesh, markers = geo2dolfin(geocode, marker_ids = markings)
    


    return mesh, markers    

def create_lv_geometry(time, ply_dir, mesh_char_len=1.0, marker_ids=None) :
    """
    Create geometry by meshing the three surfaces (endo lv, endo rv and epi)
    together using a wrapped version for gmsh
    """
    
    # Make sure the ply files excists
    for fname in ["endo_lv_{}.ply".format(time), "epi_lv_{}.ply".format(time)]:
        if not os.path.isfile("{}/{}".format(ply_dir, fname)):
            raise IOError("'{}' is not a valid .ply file.".format(\
                "{}/{}".format(ply_dir, fname)))

    from textwrap import dedent
    geocode = dedent(\
    """\
    // meshing options
    Mesh.CharacteristicLengthFromCurvature = 1;
    Mesh.Lloyd = 1;
    Geometry.HideCompounds = 0;
    Mesh.CharacteristicLengthMin = {mesh_char_len};
    Mesh.CharacteristicLengthMax = {mesh_char_len};
    Mesh.ScalingFactor = {mesh_scaling};
    Mesh.Optimize = 1;
    Mesh.OptimizeNetgen = 1;
    Mesh.RemeshParametrization = 7;  // (0=harmonic_circle, 1=conformal_spectral, 2=rbf, 3=harmonic_plane, 4=convex_circle, 5=convex_plane, 6=harmonic square, 7=conformal_fe) (Default=4)
    Mesh.SurfaceFaces = 1;
    Mesh.Algorithm    = 6; // (1=MeshAdapt, 2=Automatic, 5=Delaunay, 6=Frontal, 7=bamg, 8=delquad) (Default=2)
    Mesh.Algorithm3D    = 4; // (1=Delaunay, 4=Frontal, 5=Frontal Delaunay, 6=Frontal Hex, 7=MMG3D, 9=R-tree) (Default=1)
    Mesh.Recombine3DAll = 0;

    // load the surfaces
    Merge "{ply_dir}/endo_lv_{time}.ply";
    Merge "{ply_dir}/epi_lv_{time}.ply";

    CreateTopology;

    ll[] = Line "*";
    L_LV_base = newl; Compound Line(L_LV_base) = ll[1];
    L_epi_base = newl; Compound Line(L_epi_base) = ll[0];
    Physical Line("ENDORING") = {{ L_LV_base }};
    Physical Line("EPIRING") = {{ L_epi_base }};

    ss[] = Surface "*";
    S_LV = news; Compound Surface(S_LV) = ss[0];
    S_epi = news; Compound Surface(S_epi) = ss[1];
    Physical Surface("ENDO") = {{ S_LV }};
    Physical Surface("EPI") = {{ S_epi }};

    LL_base = newll; 
    Line Loop(LL_base) = {{ L_LV_base, L_epi_base }};
    S_base = news; Plane Surface(S_base) = {{ LL_base }};
    Physical Surface("BASE") = {{ S_base }};

    SL_wall = newsl; 
    Surface Loop(SL_wall) = {{ S_LV, S_epi, S_base }};
    V_wall = newv; Volume(V_wall) = {{ SL_wall }};
    Physical Volume("WALL") = {{ V_wall }};
    Coherence;
    """).format(time=time, ply_dir=ply_dir,
                mesh_scaling=10.0, mesh_char_len=mesh_char_len)
    # mesh_scaling = 1.0 -> cm
    # mesh_scaling = 10 -> mm
    from gmsh import geo2dolfin
    mesh, markers = geo2dolfin(geocode, marker_ids=marker_ids)
    

    return mesh, markers

def create_biv_geometry(time, ply_dir, mesh_char_len=1.0, marker_ids=None) :
    """
    Create geometry by meshing the three surfaces (endo lv, endo rv and epi)
    together using a wrapped version for gmsh
    """

    # Make sure the ply files excists
    for fname in ["endo_lv_us_{}.ply".format(time), "endo_rv_us_{}.ply".format(time), "epi_us_{}.ply".format(time)]:
        if not os.path.isfile("{}/{}".format(ply_dir, fname)):
            raise IOError("'{}' is not a valid .ply file.".format(\
                "{}/{}".format(ply_dir, fname)))

    from textwrap import dedent
    geocode = dedent(\
    """\
    // meshing options
    Mesh.CharacteristicLengthFromCurvature = 1;
    Mesh.Lloyd = 1;
    Mesh.CharacteristicLengthMin = {mesh_char_len};
    Mesh.CharacteristicLengthMax = {mesh_char_len};
    Mesh.ScalingFactor = {mesh_scaling};
    Mesh.Optimize = 1;
    Mesh.OptimizeNetgen = 1;
    Mesh.RemeshParametrization = 7;
    Mesh.SurfaceFaces = 1;


    // load the surfaces
    Merge "{ply_dir}/endo_lv_us_{time}.ply";
    Merge "{ply_dir}/endo_rv_us_{time}.ply";
    Merge "{ply_dir}/epi_us_{time}.ply";

    CreateTopology;

    ll[] = Line "*";
    L_LV_base = newl; Compound Line(L_LV_base) = ll[2];
    L_RV_base = newl; Compound Line(L_RV_base) = ll[0];
    L_epi_base = newl; Compound Line(L_epi_base) = ll[1];
    Physical Line("ENDORING_LV") = {{ L_LV_base }};
    Physical Line("ENDORING_RV") = {{ L_RV_base }};
    Physical Line("EPIRING") = {{ L_epi_base }};

    ss[] = Surface "*";
    S_LV = news; Compound Surface(S_LV) = ss[0];
    S_RV = news; Compound Surface(S_RV) = ss[1];
    S_epi = news; Compound Surface(S_epi) = ss[2];
    Physical Surface("ENDO_LV") = {{ S_LV }};
    Physical Surface("ENDO_RV") = {{ S_RV }};
    Physical Surface("EPI") = {{ S_epi }};

    LL_base = newll; 
    Line Loop(LL_base) = {{ L_LV_base, L_RV_base, L_epi_base }};
    S_base = news; Plane Surface(S_base) = {{ LL_base }};
    Physical Surface("BASE") = {{ S_base }};

    SL_wall = newsl; 
    Surface Loop(SL_wall) = {{ S_LV, S_RV, S_epi, S_base }};
    V_wall = newv; Volume(V_wall) = {{ SL_wall }};
    Physical Volume("WALL") = {{ V_wall }};

    """).format(time=time, ply_dir=ply_dir,
                mesh_scaling=10.0, mesh_char_len=mesh_char_len)
    # mesh_scaling = 1.0 -> cm
    # mesh_scaling = 10 -> mm
   
    
    from gmsh import geo2dolfin
    mesh, markers = geo2dolfin(geocode, marker_ids=marker_ids)
    

    return mesh, markers



        

### Compute/Do stuff #####
                
def refine_mesh(mesh, cell_markers=None):

    
    logger.info("\nRefine mesh")
    dolfin.parameters["refinement_algorithm"] = "plaza_with_parent_facets"

    # Refine mesh
    if cell_markers is None:
        new_mesh = dolfin.adapt(mesh)
    else:
        new_mesh = dolfin.adapt(mesh, cell_markers)

    # Refine ridges function
    # Dont work!!
    # rfun = MeshFunction("size_t", mesh, 1, mesh.domains())
    # new_rfun = adapt(rfun, new_mesh)
    
    # Refine facetfunction
    ffun = dolfin.MeshFunction("size_t", mesh, 2, mesh.domains())
    new_ffun = dolfin.adapt(ffun, new_mesh)

    # Refine cell function
    cfun = dolfin.MeshFunction("size_t", mesh, 3, mesh.domains())
    new_cfun = dolfin.adapt(cfun, new_mesh)

    # Mark the cells and facets for the new mesh
    for cell in dolfin.cells(new_mesh):
        new_mesh.domains().set_marker((cell.index(), new_cfun[cell]), 3)

        for f in dolfin.facets(cell):
            new_mesh.domains().set_marker((f.index(), new_ffun[f]), 2)

            # for e in edges(cell):
                # new_mesh.domains().set_marker((e.index(), new_rfun[e]), 1)

    return new_mesh
def refine_at_strain_regions(mesh, regions):
    strain_markers = dolfin.MeshFunction("size_t", mesh, 3, mesh.domains())

    cell_markers = dolfin.MeshFunction("bool", mesh, 3)

    if regions == "all":
        cell_markers.set_all(True)
    else:
    
        cell_markers.set_all(False)

        for c in dolfin.cells(mesh):
            if strain_markers[c] in regions:
                cell_markers[c] = True

    
    new_mesh = refine_mesh(mesh, cell_markers)
    return new_mesh


def compute_cavity_volume(mesh, endo):

    X = dolfin.SpatialCoordinate(mesh)
    N = dolfin.FacetNormal(mesh)
    ffun = dolfin.MeshFunction("size_t", mesh, 2, mesh.domains())
    ds = dolfin.Measure("exterior_facet", subdomain_data = ffun, domain = mesh)(endo)
    vol_form = (-1.0/3.0)*dolfin.dot(X, N)*ds
    return dolfin.assemble(vol_form)


### Save stuff ####
def convert_meshfunctions(h5name, h5group = ""):

    geo = load_geometry_from_h5(h5name, h5group, include_sheets=True)

    local_basis = [geo.circumferential, geo.radial, geo.longitudinal]
    fields = [geo.fiber, geo.sheet, geo.sheet_normal]

    h5name_split = os.path.splitext(h5name)
    h5name_new = "".join([h5name_split[0], "_new", h5name_split[1]])
    save_geometry_to_h5(geo.mesh, h5name_new, h5group, geo.markers, fields, local_basis)

    
def save_transformation_matrix(ply_folder, reftime, h5name):

    output_params_name = os.path.join(ply_folder, "params_{}.p".format(reftime))
    import pickle
    params = pickle.load(open(output_params_name, 'rb'))
    T = params["T_mat"]
    
        
    with h5py.File(h5name, "a") as h5file:
        h5file["{}/transformation_matrix".format(reftime)] = T

            
def save_round_off_buffer(round_off_buffer, patient, time):

    from collections import defaultdict
    # If the file allready exist, load it
    if os.path.isfile(ROUND_OFF_FILE):
        with open(ROUND_OFF_FILE, 'r') as outfile:
            dic = yaml.load(outfile)
            d = defaultdict(dict,dic)
    else:
        # Otherwise create a new one
        d = defaultdict(dict)

    d[patient][str(time)] = str(round_off_buffer)

    # Save the file
    with open(ROUND_OFF_FILE, 'w') as outfile:
        yaml.dump(dict(d), outfile, default_flow_style=False)

def add_fibers_to_h5(h5name, h5group, fields):

    with dolfin.HDF5File(dolfin.mpi_comm_world(), h5name, "a") as h5file:
        fgroup = "{}/microstructure".format(h5group)
        names = []
        for field in fields:
            name = "{}_{}".format(str(field), field.label())
            fsubgroup = "{}/{}".format(fgroup, name)
            h5file.write(field, fsubgroup)
            h5file.attributes(fsubgroup)['name'] = field.name()
            names.append(name)
       
        elm = field.function_space().ufl_element()
        family, degree = elm.family(), elm.degree()
        fspace = '{}_{}'.format(family, degree)
        h5file.attributes(fgroup)['space'] = fspace
        h5file.attributes(fgroup)['names'] = ":".join(names)

def list_fiber_angles(h5name, h5group):

    fgroup = "{}/microstructure".format(h5group)
    fibers = []
    with h5py.File(h5name, "r") as h5file:

        for key in h5file[fgroup].keys():
            if "fiber" in key:
                fibers.append(str(key))
                
    return fibers

    
def save_geometry_to_h5(mesh, h5name, h5group = "", markers = None,
                        fields = None, local_basis = None,
                        comm = None, 
                        other_functions = {}, other_attributes = {},
                        overwrite_file = False, overwrite_group=True):
    """
    Save geometry and other geometrical functions to a HDF file.

    Parameters
    ----------

    mesh: 
    


    """

    logger.info("\nSave mesh to h5")
    assert isinstance(mesh, dolfin.Mesh)
    if comm is None: comm = mesh.mpi_comm()
    file_mode = "a" if os.path.isfile(h5name) and not overwrite_file else "w"

    # IF we should append the file but overwrite the group we need to
    # check that the group does not exist. If so we need to open it in
    # h5py and delete it.
    if file_mode == "a" and overwrite_group and h5group!="":
        if has_h5py:
            check_h5group(h5name, h5group, delete = True, comm = comm)
        else:
            raise RuntimeError("Cannot overwrite group '{}'. "\
                               "Need h5py for that.".format(h5group))

    with dolfin.HDF5File(comm, h5name, file_mode) as h5file:

        # Save mesh 
        ggroup = '{}/geometry'.format(h5group)

        mgroup = '{}/mesh'.format(ggroup)


        h5file.write(mesh, mgroup)


        for i in range(4):
            mf = dolfin.MeshFunction("size_t", mesh, i, mesh.domains())
            save_mf = dolfin.MPI.max(comm, len(set(mf.array()))) > 1
            
            if save_mf:
                dgroup = '{}/mesh/meshfunction_{}'.format(ggroup, i)
                h5file.write(mf, dgroup)
            
       
        if markers is not None:
            # Save the boundary markers
            for name, (marker, dim) in markers.items():
                
                for key_str in ["domain", "meshfunction"]:

                    dgroup = '{}/mesh/{}_{}'.format(ggroup, key_str, dim)
                    
                    if h5file.has_dataset(dgroup):
                        aname = 'marker_name_{}'.format(name)
                        h5file.attributes(dgroup)[aname] = marker


        if local_basis:
            # Save local basis functions
            lgroup = "{}/local basis functions".format(h5group)
            names = []
            for l in local_basis:
                h5file.write(l, lgroup+ "/{}".format(l.name()))
                names.append(l.name())

            elm = l.function_space().ufl_element()
            family, degree = elm.family(), elm.degree()
            lspace = '{}_{}'.format(family, degree)
            h5file.attributes(lgroup)['space'] = lspace
            h5file.attributes(lgroup)['names'] = ":".join(names)
            

        if fields:
            # Save fiber field
            fgroup = "{}/microstructure".format(h5group)
            names = []
            for field in fields:
                label = field.label() if field.label().rfind('a Function') == -1 else ""
                name = "_".join(filter(None, [str(field), label]))
                fsubgroup = "{}/{}".format(fgroup, name)
                h5file.write(field, fsubgroup)
                h5file.attributes(fsubgroup)['name'] = field.name()
                names.append(name)

            elm = field.function_space().ufl_element()
            family, degree = elm.family(), elm.degree()
            fspace = '{}_{}'.format(family, degree)
            h5file.attributes(fgroup)['space'] = fspace
            h5file.attributes(fgroup)['names'] = ":".join(names)

        for k,fun in other_functions.iteritems():
            fungroup = "/".join([h5group, k])
            h5file.write(fun, fungroup)

            if isinstance(fun, dolfin.Function):
                elm = fun.function_space().ufl_element()
                family, degree, vsize= elm.family(), elm.degree(), elm.value_size()
                fspace = '{}_{}'.format(family, degree)
                h5file.attributes(fungroup)['space'] = fspace
                h5file.attributes(fungroup)['value_size'] = vsize

        for k, v in other_attributes.iteritems():
            if isinstance(v, str) and isinstance(k, str):
                h5file.attributes(h5group)[k] = v
            else:
                logger.warning("Invalid attribute {} = {}".format(k,v))

    logger.info("Geometry saved to {}".format(h5name))

    
    








